const { app, BrowserWindow } = require("electron");

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    backgroundColor: "#2a2b2c",
    webPreferences: {
      nodeIntegration: true,
    },
  });

  win.loadFile("index.html");
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

const x = (arg) => {
  if (arg == "getConf") return require("./conf.jsonc");
};

const { ipcMain } = require("electron");
ipcMain.on("asynchronous-message", (event, arg) => {
  console.log(`Recieved Message ${arg}`);
  if (arg == "getConf") event.reply("asynchronous-reply", x(arg));
});

ipcMain.on("synchronous-message", (event, arg) => {
  console.log(`Recieved Message ${arg}`);

  if (arg == "getConf") event.returnValue = x(arg);
});
